/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

/**
 *
 * @author marti
 */
public class TP1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //Crear un profesor
        Profesor p1 = new Profesor("pepe");
        
        
        //Crear dos alumnos
        
        Estudiante est1 = new Estudiante (1234,"papa");
        Estudiante est2 = new Estudiante (4567,"pipo");
        
        //Crear un curso
        
        Curso Taller1 = new Curso("Taller de Programacion Avanzada");
        
        //Asiganar el curso al profesor
        
        p1.dicta(Taller1);
        
        //Asignar Alumos al curso
        
        est1.Inscribir(Taller1);
        est2.Inscribir(Taller1);
        
        //Cuantos Alumnos hay
        
        System.out.println(Taller1.ContarInscriptos());
                
                
        
    }
    
}
