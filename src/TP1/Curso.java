/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

import java.util.ArrayList;

/**
 *
 * @author marti
 */
public class Curso {

    private String titulo;
    
    private ArrayList<Inscripcion> delAlumno;

    public Curso() {
    }

    public Curso(String titulo) {
        this.titulo = titulo;
        this.delAlumno = new ArrayList<Inscripcion>();
    }

    public ArrayList<Inscripcion> getDelAlumno() {
        return delAlumno;
    }

    public void setDelAlumno(ArrayList<Inscripcion> delAlumno) {
        this.delAlumno = delAlumno;
    }
    
    
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    
    public void addInscricion (Inscripcion i){
        this.delAlumno.add(i);
    }
    
    public int ContarInscriptos (){
        return this.delAlumno.size();
    }

}
