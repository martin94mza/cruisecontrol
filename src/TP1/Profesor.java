/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

/**
 *
 * @author marti
 */
public class Profesor extends Persona{

    public Profesor() {
    }

    public Profesor(String nombre) {
        super(nombre);
    }
    
    
    
    private Curso docente;

    public Curso getDocente() {
        return docente;
    }

    public void dicta(Curso c) {
        this.docente = c;
        
    }

}
