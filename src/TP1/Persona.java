/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

/**
 *
 * @author marti
 */
public class Persona {

    protected String nombre;

    public Persona() {
    }

    public Persona(String nombre) {
        this.nombre = nombre;
    }

    public String getString() {
        return nombre;
    }

    public void setString(String string) {
        this.nombre = string;
    }

}
