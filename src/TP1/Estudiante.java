/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

import java.util.ArrayList;

/**
 *
 * @author marti
 */
public class Estudiante extends Persona {

  
    private int legajo;

    public Estudiante() {
    }

    public Estudiante(int legajo, String nombre) {
        super(nombre);
        this.legajo = legajo;
        this.inscripto = new ArrayList<Inscripcion>();
    }

    private ArrayList<Inscripcion> inscripto;

    public ArrayList<Inscripcion> getInscripto() {
        return inscripto;
    }

    public void setInscripto(ArrayList<Inscripcion> inscripto) {
        this.inscripto = inscripto;
    }


    public int getLegajo() {
        return legajo;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    public void Inscribir (Curso c) {
        Inscripcion ins = new Inscripcion(c,this);
        this.inscripto.add(ins);
        c.addInscricion(ins);
    }
}
