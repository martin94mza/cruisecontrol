/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TP1;

/**
 *
 * @author marti
 */
public class Inscripcion {

    private Curso paraEL;
    
    private Estudiante Alumno;

    public Inscripcion() {
    }

    public Inscripcion(Curso paraEL, Estudiante Alumno) {
        this.paraEL = paraEL;
        this.Alumno = Alumno;
    }

    
    
    public Estudiante getAlumno() {
        return Alumno;
    }

    public void setAlumno(Estudiante Alumno) {
        this.Alumno = Alumno;
    }


    public Curso getParaEL() {
        return paraEL;
    }

    public void setParaEL(Curso paraEL) {
        this.paraEL = paraEL;
    }

}
